#!/bin/env python3

# Copyright © 2013–2021 Frederik “Freso” S. Olesen <https://freso.dk/>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

"""Script to assist with ripping CDs using whipper."""

import argparse
import os
import os.path
import shutil
import subprocess
import sys

from contextlib import contextmanager
from glob import glob
from tempfile import mkdtemp


def run_cmd(args,
            trim=True, quiet=False, bail_on_fail=True, live_output=False):
    """Run a command."""
    if live_output:
        res = subprocess.Popen(args, text=True,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT)
        for line in res.stdout:
            print(line.strip())
        return
    try:
        res = subprocess.run(args, check=bail_on_fail,
                             capture_output=True, text=True)
    except subprocess.CalledProcessError as e:
        print('STDOUT:\n' + e.stdout)
        print('STDERR:\n' + e.stderr)
        raise subprocess.CalledProcessError(e.returncode, e.cmd)
    if quiet:
        return
    elif trim:
        return res.stdout.strip()
    return res.stdout


@contextmanager
def _override_argv(*args):
    """Manage a separate ``sys.argv`` context."""
    _argv = sys.argv[:]
    sys.argv = list(args)
    yield
    sys.argv = _argv


def run_whipper(arguments):
    """Run whipper."""
    exit_code = None
    with _override_argv('whipper', *arguments):
        from whipper.command.main import main as whipper
        try:
            exit_code = whipper()
        except SystemExit:
            pass
    return exit_code


def run_picard(directory):
    """Run Picard."""
    with _override_argv('picard', '--no-player', directory):
        from picard.tagger import main
        try:
            main(localedir='/usr/share/locale', autoupdate=False)
        except SystemExit:
            pass


def run_regainer(filelist):
    """Run regainer."""
    with _override_argv('regainer', *filelist):
        print('DEBUG: {}'.format(sys.argv))  # TODO: Remove DEBUG
        from regainer import main as regainer
        try:
            regainer()
        except SystemExit:
            pass


def run_beet_import(directory):
    """Import into beets."""
    with _override_argv('beet', 'import', directory):
        from beets.ui import main as beet
        try:
            beet(sys.argv[1:])
        except SystemExit:
            pass


def main(args):
    """Run the script workflow."""

    parser = argparse.ArgumentParser()
    parser.add_argument('-t', dest='TMPDIR', default=None)
    parser.add_argument('-d', dest='DESTDIR', default=os.getcwd())
    parser.add_argument('-r', dest='RELEASE_ID', default='')
    parser.add_argument('-c', dest='DRIVE', default='/dev/cdrom')
    parser.add_argument('-a', dest='MAX_RETRIES', default=5)
    args = parser.parse_args(args)

    if not args.TMPDIR:
        args.TMPDIR = mkdtemp(prefix='rip-disc.')
        print('Setting TMPDIR to {}.'.format(args.TMPDIR))
    if args.DESTDIR != parser.get_default('DESTDIR'):
        print('Setting DESTDIR to {}.'.format(args.DESTDIR))
    if args.RELEASE_ID != parser.get_default('RELEASE_ID'):
        print('Setting RELEASE_ID to {}.'.format(args.RELEASE_ID))
        print('... See release on MusicBrainz:',
              'https://musicbrainz.org/release/{}'.format(args.RELEASE_ID))
    if args.DRIVE != parser.get_default('DRIVE'):
        print('Using disk drive {}.'.format(args.DRIVE))

    TRACK_TEMPLATE = '%A - %d (%y) [%X]/%t. %a - %n'
    DISC_TEMPLATE = '%A - %d (%y) [%X]/%A - %d'

    whipper_arguments = (
        'cd', "--device={}".format(args.DRIVE),
        'rip', '--cdr', '--prompt', '--keep-going',
        "--max-retries={}".format(args.MAX_RETRIES),
        "--working-directory={}".format(args.TMPDIR),
        "--output-directory=''",
        "--track-template={}".format(TRACK_TEMPLATE),
        "--disc-template={}".format(DISC_TEMPLATE),
        "--release-id={}".format(args.RELEASE_ID),
    )

    print('Ripping to:', args.TMPDIR)

    run_cmd(('eject', '--trayclose', args.DRIVE),
            quiet=True, bail_on_fail=False)

    print('... Ripping the CD.')
    print('    Using: whipper {}'.format(' '.join(whipper_arguments)))

    run_whipper(whipper_arguments)

    # TODO: More error checking, e.g., if there are more than 1 item in TMPDIR
    discdir = os.path.join(args.TMPDIR, os.listdir(args.TMPDIR)[0])
    filelist = tuple(glob(os.path.join(discdir, '**.flac'), recursive=True))
    print('... Working with {}.'.format(discdir))

    print('... Recompressing.')
    run_cmd((
        'flac', '--preserve-modtime', '--best', '--verify', '--force'
    ) + filelist, live_output=True)

    print('... Calculating ReplayGain.')
    run_regainer(filelist)

    print('... Additionally tag files.')
    run_picard(discdir)

    # print('... Generating torrent file.')
    # run_cmd(('mktor', '...'), trim=False)

    print('... Compress into archive.')
    archive = discdir + '.7z'
    run_cmd((
        '7z', 'a', '-t7z',
        '-m0=lzma2', '-mx=9', '-mfb=64', '-md=32m', '-ms=on', '-mmt=on',
        archive, discdir
    ), live_output=True)

    print('... Moving archive.')
    run_cmd((
        'rsync', '-avzz', '--remove-sent-files', '--progress',
        archive, args.DESTDIR,
    ), live_output=True)

    print('... Importing to beets.')
    while True:
        beet_question = '    Do you want to import {} into beets? (yes, no) '
        beet = input(beet_question.format(discdir)).strip()
        if beet.lower() in ('y', 'yes'):
            print('    ... Importing...')
            run_beet_import(discdir)
            break
        elif beet.lower() in ('n', 'no'):
            print('    ... Not importing.')
            break
        else:
            print('    ... “{}” not understood.'.format(beet))

    print('... Cleaning up.')
    while True:
        clean_question = '    Do you want to remove {}? (yes, no) '
        clean = input(clean_question.format(args.TMPDIR)).strip()
        if clean.lower() in ('y', 'yes'):
            print('    ... Removing...')
            shutil.rmtree(args.TMPDIR)
            break
        elif clean.lower() in ('n', 'no'):
            print('    ... Not removing.')
            break
        else:
            print('    ... “{}” not understood.'.format(clean))


if __name__ == '__main__':
    main(sys.argv[1:])
